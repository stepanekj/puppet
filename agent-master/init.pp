
$vpcName = 'agent-master'
$ensure = 'present'
# $ensure = 'absent'

$windowsName = 'windows-agent'
$windowsAmi = 'ami-02a14e6d'

$debianName = 'debian-tester'
$debianAmi = 'ami-114da17e'

$debianMasterName = 'debian-master'
$debianMasterAmi = 'ami-c7a24da8'

aws_vpc { $vpcName:
  ensure  => $ensure,
  vpcName => $vpcName,
}

aws_vpc::ec2_instance { 'windows-agent':
  name     => $windowsName,
  image_id => $windowsAmi,
}

aws_vpc::ec2_instance { 'debian-tester':
  name     => $debianName,
  image_id => $debianAmi,
}

aws_vpc::ec2_instance { 'debian-master':
  name     => $debianMasterName,
  image_id => $debianMasterAmi,
}


Aws_vpc::Ec2_instance <||> {
  ensure => $ensure,
  subnet => "${vpcName}-subnet",
  security_groups => ["${vpcName}-access"]
}


aws_vpc::ec2_elastic_ip { '52.29.188.156':
  ensure   => $ensure,
  instance => $windowsName
}
aws_vpc::ec2_elastic_ip { '52.28.112.152':
  ensure   => $ensure,
  instance => $debianName
}

aws_vpc::ec2_elastic_ip { '52.28.187.150':
  ensure   => $ensure,
  instance => $debianMasterName
}

if $ensure == 'absent' {
  Ec2_instance <||> {
    before => [
      Ec2_vpc_subnet["${vpcName}-subnet"],
      Ec2_securitygroup["${vpcName}-access"]
    ]
  }
}
