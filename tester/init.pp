package { ['ruby', 'ruby-dev']:
  ensure => latest,
  provider => apt
}

package { ['rspec-support', 'rspec', 'serverspec', 'rake', 'winrm']:
  ensure => latest,
  provider => gem
}
