require 'serverspec'
require 'winrm'

set :backend, :winrm
set :os, :family => 'windows', :release => '2012', :arch => 'x64'


user = "puppet"
pass = "11A2bc3115"
endpoint = "http://#{ENV['TARGET_HOST']}:5985/wsman"

winrm = ::WinRM::WinRMWebService.new(endpoint, :plaintext, :user => user, :pass => pass, :basic_auth_only => true)
winrm.set_timeout 300 # 5 minutes max timeout for any operation
Specinfra.configuration.winrm = winrm
