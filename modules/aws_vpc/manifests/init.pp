define aws_vpc(
  $vpcName,
  $cidr_block = '10.0.0.0/24',
  $ensure
) {
  ec2_vpc { $vpcName:
    ensure       => $ensure,
    region       => 'eu-central-1',
    cidr_block   => $cidr_block
  }

  ec2_securitygroup { "${vpcName}-access":
    ensure      => $ensure,
    region      => 'eu-central-1',
    vpc         => $vpcName,
    description => 'Security group for VPC',
    ingress     => [{
      security_group => "${vpcName}-access",
    },{
      protocol => 'tcp',
      port     => 22, #ssh
      cidr     => '0.0.0.0/0'
    },{
      protocol => 'tcp',
      port     => 3389, #rdp
      cidr     => '0.0.0.0/0'
    }],
    before => $ensure ? {
      'present' => undef,
      'absent' => [
        Ec2_vpc["${vpcName}"]
      ]
    }
  }

  ec2_vpc_subnet { "${vpcName}-subnet":
    ensure            => $ensure,
    region            => 'eu-central-1',
    vpc               => $vpcName,
    cidr_block        => '10.0.0.0/28',
    availability_zone => 'eu-central-1a',
    route_table       => "${vpcName}-routes",
    before => $ensure ? {
      'present' => undef,
      'absent' => [
        Ec2_vpc["${vpcName}"],
        Ec2_vpc_routetable["${vpcName}-routes"]
      ]
    }
  }

  ec2_vpc_internet_gateway { "${name}-igw":
    ensure => $ensure,
    region => 'eu-central-1',
    vpc    => $vpcName,
    before => $ensure ? {
      'present' => undef,
      'absent' => [
        Ec2_vpc["${vpcName}"]
      ]
    }
  }

  ec2_vpc_routetable { "${vpcName}-routes":
    ensure => $ensure,
    region => 'eu-central-1',
    vpc    => $vpcName,
    routes => [
      {
        destination_cidr_block => $cidr_block,
        gateway                => 'local'
      },{
        destination_cidr_block => '0.0.0.0/0',
        gateway                => "${vpcName}-igw"
      },
    ],
    before => $ensure ? {
      'present' => undef,
      'absent' => [
        Ec2_vpc_internet_gateway["${vpcName}-igw"],
        Ec2_vpc["${vpcName}"]
      ]
    }
  }
}