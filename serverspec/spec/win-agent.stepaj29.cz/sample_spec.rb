require 'spec_helper'


describe file('c:/temp/test.txt') do
  it { should be_file }
  it { should contain "some text" }
end

describe user('puppet') do
  it { should exist }
  it { should belong_to_group('Administrators')}
end

describe package('Git version 2.8.3') do
  it { should be_installed }
end

describe windows_feature('Powershell') do
  it { should be_installed.by("powershell").with_version("4.0") }
end


describe command('Get-ExecutionPolicy') do
  its(:stdout) { should contain 'Unrestricted'}
end

describe port(3389) do
  it { should be_listening }
end

describe service('DNS Client') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
  it { should have_start_mode("Automatic") }
end

describe file('C:\Users') do
  it { should be_directory }
  it { should be_readable }
  it { should_not be_writable.by('Everyone') }
end



