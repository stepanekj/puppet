define aws_vpc::ec2_elastic_ip (
  $ensure,
  $instance
) {
  $state = $ensure ? {
    'present' => 'attached',
    'running' => 'attached',
    'stopped'  => 'detached',
    'absent'  => 'detached',
    default => 'detached'
  }
  ec2_elastic_ip { $name:
    ensure => $state,
    region => 'eu-central-1',
    instance => $instance
  }
}