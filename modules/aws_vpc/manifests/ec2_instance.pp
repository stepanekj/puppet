define aws_vpc::ec2_instance (
  $image_id,
  $subnet,
  $ensure,
  $security_groups,
  $instance_type = 't2.small'
) {
  ec2_instance { "${name}":
    ensure  => $ensure,
    region  => 'eu-central-1',
    security_groups => $security_groups,
    key_name  => 'stepaj29-puppet',
    instance_type => $instance_type,
    image_id => $image_id,
    subnet  => $subnet,
    tags  => {
      'Owner' => 'stepaj29'
    },

  }
}