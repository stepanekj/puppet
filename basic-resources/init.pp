
$vpcName = 'basic-resources'
# $ensure = 'present'
$ensure = 'absent'

$windowsName = 'windows-agent'
$windowsAmi = 'ami-024ea26d'

$debianName = 'debian-tester'
$debianAmi = 'ami-114da17e'

aws_vpc { $vpcName:
  ensure  => $ensure,
  vpcName => $vpcName,
}

aws_vpc::ec2_instance { 'windows':
  name     => $windowsName,
  image_id => $windowsAmi,
}

aws_vpc::ec2_instance { 'debian':
  name     => $debianName,
  image_id => $debianAmi,
}

Aws_vpc::Ec2_instance <||> {
  ensure => $ensure,
  subnet => "${vpcName}-subnet",
  security_groups => ["${vpcName}-access"]
}


aws_vpc::ec2_elastic_ip { '52.29.188.156':
  ensure   => $ensure,
  instance => $windowsName
}
aws_vpc::ec2_elastic_ip { '52.28.112.152':
  ensure   => $ensure,
  instance => $debianName
}

if $ensure == 'absent' {
  Ec2_instance <||> {
    before => [
      Ec2_vpc_subnet["${vpcName}-subnet"],
      Ec2_securitygroup["${vpcName}-access"]
    ]
  }
}
